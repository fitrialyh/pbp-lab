from django.http.response import HttpResponse 
from django.core import serializers

from lab_2.models import Note
from django.shortcuts import render

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(request):
    notes = Note.objects.all()
    notes = serializers.serialize('xml', notes)
    return HttpResponse(notes, content_type="application/xml")

def json(request):
    notes = Note.objects.all()
    notes = serializers.serialize('json', notes)
    return HttpResponse(notes, content_type="application/json")