from django import forms
from lab_4.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"

        to = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput)
        From = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput)
        title = forms.CharField(label='', required=True, max_length=50, widget=forms.TextInput)
        message = forms.CharField(label='', required=True, max_length=300, widget=forms.Textarea)