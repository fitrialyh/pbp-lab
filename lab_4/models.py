from django.db import models

class Note(models.Model):
    to = models.CharField(max_length=30)
    From = models.CharField(max_length=30)
    title = models.CharField(max_length=50)
    message = models.TextField(max_length=500)