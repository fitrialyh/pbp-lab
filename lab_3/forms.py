from django import forms
from lab_3.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"

        name = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput())
        npm = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput())
        dob = forms.DateField(label='', required=True, widget=forms.DateInput())
