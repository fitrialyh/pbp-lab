from django.db import models
from django.contrib.auth.models import User

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=30)
    dob = models.DateField()