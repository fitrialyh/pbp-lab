# Apakah perbedaan antara JSON dan XML?
JSON (JavaScript Object Notation) didesain menjadi self-describing. Sedangkan, XML (eXtensible Markup Language) didesain menjadi self-descriptive.

Sintaks JSON merupakan turunan dari Object JavaScript. Akan tetapi tetapi format JSON berbentuk text, sehingga kode untuk membaca dan membuat JSON banyak terdapat dibanyak bahasa pemrograman. Sedangkan, XML adalah bahasa markup yang hanyalah berisi informasi yang dibungkus di dalam tag. Kita perlu menulis program untuk mengirim, menerima, menyimpan, atau menampilkan informasi tersebut.

JSON lebih digunakan untuk pengiriman data antara server dan browser. Selain itu, JSON lebih cepat dan mudah untuk digunakan untuk aplikasi AJAX. Sementara untuk XML lebih digunakan untuk menyimpan data pada server-side. Jika pertukaran data akan berjalan secara kompleks maka XML digunakan dan jika sebaliknya JSON yang akan digunakan.

# Apakah perbedaan antara HTML dan XML?
HTML dan XML dirancang untuk tujuan yang berbeda, XML dirancang untuk membawa data dengan fokus apa data tersebut, sedangkan HTML dirancang untuk menampilkan data dengan fokus bagaimana tampilan data tersebut. Selain itu, tags pada XML tidak predefined seperti HTML.